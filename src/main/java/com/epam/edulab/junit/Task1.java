package com.epam.edulab.junit;

public class Task1 {
    public String concatenateWords(String first, String second) {
        return first.concat(second);
    }
    public int computeFactorial(int n) {
        return (n == 0) ? 1 : n * computeFactorial(n - 1);
    }
}