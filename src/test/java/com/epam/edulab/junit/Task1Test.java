package com.epam.edulab.junit;

import org.junit.Test;
import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotNull;


public class Task1Test {
    @Test
    public void concatenateWordsTest() {
        Task1 test = new Task1();
        String first = "first";
        String second = " test";
        String concatenate = test.concatenateWords(first, second);
        assertEquals(concatenate, "first test");
        assertNotNull(concatenate);
    }

    @Test
    public void computeFactorialTest() {
        Task1 test = new Task1();
        int a = 4;
        int result = test.computeFactorial(a);
        assertEquals(result, 24);
    }

    @Test(timeout = 1000)
    public void computeFactorialTimeout() {
        Task1 test = new Task1();
        int a = 3;
        int result = test.computeFactorial(a);
        assertEquals(result, 6);
    }
}
